package chapter7.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(urlPatterns= {"/editUser", "/index.jsp", "/signup", "/userManagement"})
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
			System.out.println("ログインフィルター");


			HttpSession session = ((HttpServletRequest)request).getSession();

		        if (session.getAttribute("loginUser") == null) {

		            ((HttpServletResponse)response).sendRedirect("login");
		}else {

		chain.doFilter(request, response); // サーブレットを実行

		}
	}

	@Override
	public void init(FilterConfig config)throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
