package chapter7.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;

@WebFilter(urlPatterns = { "/editUser", "/signup", "/userManagement"})
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		System.out.println("権限フィルター");

		List<String>errorMessage = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest) request).getSession();

		//User型の　getter setterメソッドを使うためにUser形にキャストする
		User user = (User) session.getAttribute("loginUser");


	String branchIdprm = user.getBranchId();
	String departmentIdprm = user.getDepartmentId();

	int branchId = Integer.parseInt(branchIdprm);
	int departmentId = Integer.parseInt(departmentIdprm);

		if(branchId==1&&departmentId==1){
			chain.doFilter(request, response); // サーブレットを実行

		}else{
			errorMessage.add("権限がありません");
			session.setAttribute("errorMessage" , errorMessage);
			((HttpServletResponse) response).sendRedirect("index.jsp");

		}
	}

	@Override
	public void init(FilterConfig config)throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
