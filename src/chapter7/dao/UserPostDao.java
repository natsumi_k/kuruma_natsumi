package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserPost;
import chapter7.exception.SQLRuntimeException;

public class UserPostDao {

	//投稿一覧取得のためのSQL文、日付指定の条件を追加
	public List<UserPost> getUserPosts(Connection connection, int num, String starttime, String endtime, String category){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("posts.id as id,");
			sql.append("posts.subject as subject, ");
			sql.append("posts.text as text, ");
			sql.append("posts.category as category, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("posts.created_date as created_date ");
			sql.append("FROM posts ");
			sql.append("INNER JOIN users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("WHERE posts.created_date BETWEEN ? AND ? ");
			sql.append("AND posts.category LIKE ? ");
			sql.append("ORDER BY posts.created_date DESC limit "+ num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, starttime);
			ps.setString(2, endtime);
			ps.setString(3, "%"+category+"%");

			System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserPostList(rs);
			return ret;
		 } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	     } finally {
	            close(ps);
	     }

	}

	private List<UserPost> toUserPostList(ResultSet rs) throws SQLException{

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while(rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserPost post = new UserPost();
				post.setLogin_id(loginId);
				post.setName(name);
				post.setId(id);
				post.setUser_id(userId);
				post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setCreated_date(createdDate);

				ret.add(post);
			  }
            return ret;
        } finally {
            close(rs);
        }
    }
}
