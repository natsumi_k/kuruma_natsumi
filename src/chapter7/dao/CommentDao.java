package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Comment;
import chapter7.exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {


		System.out.println("insert");

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("user_id");
			sql.append(", post_id");
			sql.append(", text");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // user_id
			sql.append(", ?"); // post_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUser_id());
			ps.setInt(2, comment.getPost_id());
			ps.setString(3, comment.getText());

			ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void deleteComments(Connection connection, Comment comment){

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE ");
			sql.append("FROM comments ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getId());


			ps.executeUpdate();

		 } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	     } finally {
	            close(ps);
	     }
	}
}