package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.exception.SQLRuntimeException;


public class UserDao {
	public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", is_working");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?"); // is_working
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginID());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranchId());
            ps.setString(5, user.getDepartmentId());
            ps.setInt(6, user.getIsWorking());

            System.out.println(ps.toString());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	public User getUser(Connection connection, String loginId,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, loginId);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            String branchId = rs.getString("branch_id");
	            String departmentId = rs.getString("department_id");
	            Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");
	            int isWorking = rs.getInt("is_working");

	            User user = new User();
	            user.setId(id);
	            user.setLoginID(loginId);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranchId(branchId);
	            user.setDepartmentId(departmentId);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);
	            user.setIsWorking(isWorking);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public List<User> getUserList(Connection connection) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users";

	        ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else {
	            return userList;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			 } else if (2 <= userList.size()) {
		            throw new IllegalStateException("2 <= userList.size()");
		        } else {
		            return userList.get(0);
		        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	//パスワード変更なければ初期値で変更
	public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
        	if(!user.getPassword().isEmpty()) {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append(" login_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginID());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getBranchId());
            ps.setString(5, user.getDepartmentId());
            ps.setInt(6, user.getid());

        	}else{
            	 StringBuilder sql1 = new StringBuilder();
            	 sql1.append("UPDATE users SET");
                 sql1.append(" login_id = ?");
                 sql1.append(", name = ?");
                 sql1.append(", branch_id = ?");
                 sql1.append(", department_id = ?");
                 sql1.append(", updated_date = CURRENT_TIMESTAMP");
                 sql1.append(" WHERE");
                 sql1.append(" id = ?");


                 ps = connection.prepareStatement(sql1.toString());

                 ps.setString(1, user.getLoginID());
                 ps.setString(2, user.getName());
                 ps.setString(3, user.getBranchId());
                 ps.setString(4, user.getDepartmentId());
                 ps.setInt(5, user.getid());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }


	//ユーザーの停止復活のUpdateメソッド
	public void changeState(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
            sql.append(" is_working = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, user.getIsWorking());
            ps.setInt(2, user.getid());

            System.out.print(ps.toString());

            int count = ps.executeUpdate();
            if(count == 0) {
            	throw new NoRowsUpdatedRuntimeException();
            }
	 	} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

	}

}
