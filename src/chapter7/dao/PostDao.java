package chapter7.dao;


import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter7.beans.Post;
import chapter7.exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts( ");
			sql.append("user_id");
			sql.append(", subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(")VALUES(");
			sql.append(" ?");//user_id
			sql.append(", ?");//subject
			sql.append(", ?");//text
			sql.append(", ?");//category
			sql.append(", CURRENT_TIMESTAMP");//created_date
			sql.append(", CURRENT_TIMESTAMP");//updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, post.getUserId());
			ps.setString(2, post.getSubject());
			ps.setString(3, post.getText());
			ps.setString(4, post.getCategory());

			ps.executeUpdate();
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}

	}

	public void deletePosts(Connection connection,Post post) {

		PreparedStatement ps = null;

		try {
			String sql = "DELETE FROM posts WHERE id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, post.getId());

			System.out.println(ps.toString());

			ps.executeUpdate();

		}catch (SQLException e){
			throw new SQLRuntimeException(e);
	    } finally {
	           close(ps);
	    }
	}
}