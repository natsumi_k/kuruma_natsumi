package chapter7.dao;

import static chapter7.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter7.beans.UserComment;
import chapter7.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num){

		System.out.println("select");

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.user_id as user_id, ");
			sql.append("comments.post_id as post_id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("comments.created_date as created_date ");
			sql.append("FROM comments ");
			sql.append("INNER JOIN users ");
			sql.append("ON comments.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserComment>toUserCommentList(ResultSet rs)throws SQLException {
		List<UserComment>ret =new ArrayList<UserComment>();
		try {
		while(rs.next()) {
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			int id = rs.getInt("id");
			int userId = rs.getInt("user_id");
			int postId = rs.getInt("post_id");
			String text = rs.getString("text");
			Timestamp createdDate = rs.getTimestamp("created_date");

			UserComment comment = new UserComment();
			comment.setLoginId(loginId);
			comment.setName(name);
			comment.setId(id);
			comment.setUserId(userId);
			comment.setPostId(postId);
			comment.setText(text);
			comment.setCreated_date(createdDate);

			ret.add(comment);

			}
            return ret;
        } finally {
            close(rs);
        }
    }
}






