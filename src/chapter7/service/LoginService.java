package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;

import chapter7.beans.User;
import chapter7.dao.UserDao;
import chapter7.utils.CipherUtil;

public class LoginService {
	public User login(String loginId , String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, loginId, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}

