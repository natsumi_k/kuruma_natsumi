package chapter7.service;

import static chapter7.utils.CloseableUtil.*;
import static chapter7.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter7.beans.Post;
import chapter7.beans.UserPost;
import chapter7.dao.PostDao;
import chapter7.dao.UserPostDao;

public class PostService {

	public void register(Post post) {


		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	//投稿一覧をとってくる。＋取得する日付の条件の指定
	public List<UserPost> getPost(String starttime,String endtime, String category){

		Connection connection = null;
		try {
			connection = getConnection();

			UserPostDao postDao = new UserPostDao();

			if (starttime == null || starttime.equals("")){
				starttime = "2019-01-01 00:00:00";
		    }else{
		    	starttime = starttime + " 00:00:00";
		    }

		    if (endtime == null || endtime.equals("")) {
		    	endtime = "2019-12-31 23:59:59";
		    }else {
		    	endtime = endtime + " 23:59:59";
		    }

		    if(category == null) {
		    	category = "";
		    }


			List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM,starttime,endtime,category);

			commit(connection);


			 return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	public void deletePosts(Post post) {
		System.out.println("postdelete");

		Connection connection =null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.deletePosts(connection,post);

			commit(connection);

        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}