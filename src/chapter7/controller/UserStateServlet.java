package chapter7.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.User;
import chapter7.service.UserService;


@WebServlet(urlPatterns = {"/userState"})
public class UserStateServlet extends HttpServlet{
	private static final long serialVersionUID =1L;
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{

		User user = new User();

		//beanからIsworkingとIdを取得
		user.setIsWorking(Integer.parseInt(request.getParameter("isWorking")));
		user.setId(Integer.parseInt(request.getParameter("userstateid")));

		//userserviceに接続
		new UserService().changeState(user);

		response.sendRedirect("userManagement");
	}
}
