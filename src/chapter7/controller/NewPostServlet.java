package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Post;
import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.service.PostService;


@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		    request.getRequestDispatcher("/newpost.jsp").forward(request, response);

	}

	@Override//投稿の入力
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws IOException,ServletException{

		List<String> errorMessage = new ArrayList<String>();
		HttpSession session = request.getSession();
		Post post = getPost(request);


		if(isValid(request,errorMessage) == true){//validationでエラーなし

				try {

				new PostService().register(post);
			}catch(NoRowsUpdatedRuntimeException e){
				session.setAttribute("errorMessage", errorMessage);
				request.setAttribute("post",post);
				request.getRequestDispatcher("newpost.jsp").forward(request, response);
			}
			response.sendRedirect("./");
		} else {
			request.setAttribute("errorMessage" , errorMessage);
			request.setAttribute("post", post);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);

		}


	}
	private Post getPost(HttpServletRequest request)throws IOException, ServletException{

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		Post post = new Post();

		post.setSubject(request.getParameter("subject"));
		post.setText(request.getParameter("text"));
		post.setCategory(request.getParameter("category"));
		post.setUserId(user.getid());
		return post;

	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessage) {

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");



		if(StringUtils.isEmpty(subject) == true) {
			errorMessage.add("件名を入力してください");
		}
		if(StringUtils.isEmpty(text) == true) {
			errorMessage.add("本文を入力してください");
		}
		if(StringUtils.isEmpty(category) == true) {
			errorMessage.add("カテゴリーを入力してください");
		}
		if(errorMessage.size() == 0) {
			return true;
		}else{
			return false;
		}
	}
}
