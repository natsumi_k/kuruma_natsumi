package chapter7.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.Post;
import chapter7.service.PostService;

@WebServlet(urlPatterns = {"/postDelete"})
public class PostDeleteServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		System.out.println("doPost");

		Post post = new Post();
		post.setId(Integer.parseInt(request.getParameter("id")));

		new PostService().deletePosts(post);

		response.sendRedirect("./");
	}
}
