package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Branch;
import chapter7.beans.Department;
import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.service.BranchService;
import chapter7.service.DepartmentService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = {"/editUser"})
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	//すでに登録してあるユーザー情報を表示する
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{


		//UserBeanのIdを取得する（編集したい人は一人なのでIdも一つ）
        User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("editid")));


		//editUserをKeyとしてedituserのIdをrequest領域にセットする
		request.setAttribute("editUser", editUser);

		BranchService service = new BranchService();
		List<Branch>branches = service.getBranches();

		request.setAttribute("branches",branches);

		DepartmentService service2 = new DepartmentService();
		List<Department>departments = service2.getDepartments();

		request.setAttribute("departments", departments);


		//edituser.jspを表示
		request.getRequestDispatcher("edituser.jsp").forward(request, response);
	}

	@Override//ユーザー情報を変更する
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException{
		System.out.println("dopost");

		//errorMessageをList化
		List<String> errorMessage = new ArrayList<String>();
		HttpSession session = request.getSession();
		User rewriteUser = getEditUser(request);//userが書き換えたユーザーの情報
		BranchService service = new BranchService();
		List<Branch>branches = service.getBranches();

		request.setAttribute("branches",branches);

		DepartmentService service2 = new DepartmentService();
		List<Department>departments = service2.getDepartments();

		request.setAttribute("departments", departments);

		if(isValid(request,errorMessage) == true) {//validationのerrorが何もなかったら

			try {
				//userServiceの更新メソッド呼び出し、書き換え
				new UserService().update(rewriteUser);
			}catch(NoRowsUpdatedRuntimeException e){
				errorMessage.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");

				//エラーメッセージをセッション領域にセット
				session.setAttribute("errorMessage", errorMessage);
				//EdituserをKeyにして書き換えたRewriteuserをrequest領域にセット
				request.setAttribute("editUser",rewriteUser);
				//編集画面のまま
				request.getRequestDispatcher("edituser.jsp").forward(request, response);
				return;
			}
			response.sendRedirect("userManagement");
		}else{
			session.setAttribute("errorMessage", errorMessage);
			request.setAttribute("editUser", rewriteUser);
			request.getRequestDispatcher("edituser.jsp").forward(request, response);
		}
	}
	private User getEditUser(HttpServletRequest request)throws IOException, ServletException{


		User rewriteUser = new User();//書き換えたユーザー情報を画面に表示
		rewriteUser.setId(Integer.parseInt(request.getParameter("id")));
		rewriteUser.setLoginID(request.getParameter("loginID"));
		rewriteUser.setPassword(request.getParameter("password"));
		rewriteUser.setName(request.getParameter("name"));
		rewriteUser.setBranchId(request.getParameter("branchId"));
		rewriteUser.setDepartmentId(request.getParameter("departmentId"));
		return rewriteUser;
	}



	private boolean isValid(HttpServletRequest request, List<String> errorMessage) {
		String loginId = request.getParameter("loginID");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("password_check");
		String name = request.getParameter("name");

		if (StringUtils.isEmpty(loginId) == true) {
			errorMessage.add("ログインIDを入力してください");
        }
		if(loginId.matches("^[0-9a-zA-Z]{6,20}$") != true) {
			errorMessage.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		}
		if(password.matches("^[^ -~｡-ﾟ]{6,20}$") == true) {
			errorMessage.add("パスワードは記号を含む半角英数字6文字以上20文字以下で入力してください");
		}
		if(!(password.equals(passwordCheck))) {
			errorMessage.add("パスワードが一致していません");
		}
		if (StringUtils.isEmpty(name) == true) {
			errorMessage.add("名前を入力してください");
        }
		if(name.length() >= 10) {
			errorMessage.add("名前は10文字以内で入力してください");
		}
		//アカウントがすでに利用されていないか、メールアドレスがすでに登録されていないか確認
		if (errorMessage.size() == 0) {
            return true;
        } else {
            return false;
        }

	}

}