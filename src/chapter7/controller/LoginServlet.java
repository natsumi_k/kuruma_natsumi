package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter7.beans.User;
import chapter7.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User loginuser = loginService.login(loginId, password);

        User user = getUser(request);
        HttpSession session = request.getSession();


        //ユーザー情報がnullかどうか確認する
        if (loginuser != null) {
        	 //Isworkingが0（停止）の場合ログインできない
        	 if(loginuser.getIsWorking() == 0) {
             	List<String> messages = new ArrayList<String>();
                messages.add("ログインできません。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("user",user);
                request.getRequestDispatcher("login.jsp").forward(request, response);
             	return;
             }

        	//Userがいればセッション領域にログイン情報（Id,Passを保存）
            session.setAttribute("loginUser", loginuser);
            response.sendRedirect("./");

        	} else {
	            List<String> messages = new ArrayList<String>();
	            messages.add("ログインに失敗しました。");
	            session.setAttribute("errorMessages", messages);
	            request.setAttribute("user",user);
	            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
     }
    private User getUser(HttpServletRequest request)throws IOException, ServletException{
    	//user情報をJSPに渡す
		User user = new User();
		user.setLoginID(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		return user;
	}
}