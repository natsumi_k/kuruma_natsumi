package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.UserComment;
import chapter7.beans.UserPost;
import chapter7.service.CommentService;
import chapter7.service.PostService;




@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


    	// new CommentService().getCreatedDate(request.getParameter("start"),request.getParameter("end"));



    	 List<UserPost>posts = new PostService().getPost(request.getParameter("start"),request.getParameter("end"),request.getParameter("category"));
         List<UserComment>comments = new CommentService().getComment();

         request.setAttribute("posts", posts);
         request.setAttribute("comments", comments);

         request.getRequestDispatcher("/home.jsp").forward(request, response);

 //   	new CommentService().getCreatedDate;

 //   	String start = request.getParameter("start");
 //  	String end = request.getParameter("end");


	}
}

