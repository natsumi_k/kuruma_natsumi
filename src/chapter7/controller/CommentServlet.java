package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Comment;
import chapter7.beans.User;
import chapter7.service.CommentService;


	@WebServlet(urlPatterns = { "/comment" })
	public class CommentServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;


		@Override
		protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
			System.out.println("doPost");

		HttpSession session = request.getSession();
		List<String>errorMessages = new ArrayList<String>();


			if(isValid(request,errorMessages) == true){


				User user = (User) session.getAttribute("loginUser");
				Comment comment = new Comment();

				comment.setText(request.getParameter("text"));
				comment.setPost_id(Integer.parseInt(request.getParameter("post_id")));
				comment.setUser_id(user.getid());


				new CommentService().register(comment);

				session.setAttribute("errorMessage", errorMessages);
				response.sendRedirect("./");
			} else {
				System.out.println("doPost if false");

				session.setAttribute("errorMessages" , errorMessages);
				response.sendRedirect("index.jsp");
			}
		}

		private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

			String comment = request.getParameter("text");

			if(StringUtils.isEmpty(comment) == true) {
				errorMessages.add("コメントを入力してください");
			}

			if(errorMessages.size() == 0) {
				return true;
			}else{
				return false;
			}
		}
	}


