package chapter7.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter7.beans.Branch;
import chapter7.beans.Department;
import chapter7.beans.User;
import chapter7.service.BranchService;
import chapter7.service.DepartmentService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = {"/userManagement"})
public class UserManagementServlet extends HttpServlet{
	private static final long serialVersionUID =1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException{
		System.out.println("doget");

		List<User>users = new UserService().getUser();

		BranchService service = new BranchService();
		List<Branch>branches = service.getBranches();

		request.setAttribute("branches",branches);

		DepartmentService service2 = new DepartmentService();
		List<Department>departments = service2.getDepartments();

		request.setAttribute("departments", departments);


		request.setAttribute("users", users);
		request.getRequestDispatcher("/usermanagement.jsp").forward(request, response);
	}
}

