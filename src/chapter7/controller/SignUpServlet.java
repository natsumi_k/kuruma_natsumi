package chapter7.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter7.beans.Branch;
import chapter7.beans.Department;
import chapter7.beans.User;
import chapter7.exception.NoRowsUpdatedRuntimeException;
import chapter7.service.BranchService;
import chapter7.service.DepartmentService;
import chapter7.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		System.out.println("doget");

		//branch情報をJSPに渡す

		BranchService service = new BranchService();
		List<Branch>branches = service.getBranches();
		request.setAttribute("branches",branches);

		DepartmentService service2 = new DepartmentService();
		List<Department>departments = service2.getDepartments();
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		System.out.println("doPost");

		List<String>errorMessage = new ArrayList<String>();
		HttpSession session = request.getSession();
		User user = getUser(request);

		BranchService service = new BranchService();
		List<Branch>branches = service.getBranches();
		request.setAttribute("branches",branches);

		DepartmentService service2 = new DepartmentService();
		List<Department>departments = service2.getDepartments();
		request.setAttribute("departments", departments);

		if(isValid(request, errorMessage) == true) {

			try {
				new UserService().register(user);

				}catch(NoRowsUpdatedRuntimeException e){
				session.setAttribute("errorMessage", errorMessage);
				request.setAttribute("user",user);
				request.getRequestDispatcher("signup.jsp").forward(request, response);
			}
			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessage" , errorMessage);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}
	private User getUser(HttpServletRequest request)throws IOException, ServletException{

		//userが入力した情報をbeanに持っていく→表示する
		User user = new User();
		user.setLoginID(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(request.getParameter("branchId"));
		user.setDepartmentId(request.getParameter("departmentId"));
		return user;

	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessage) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCheck =request.getParameter("password_check");
		String name = request.getParameter("name");

		if(StringUtils.isEmpty(loginId) == true ) {
			errorMessage.add("ログインIDを入力してください");
		}
		if(loginId.matches("^[0-9a-zA-Z]{6,20}$") != true) {
			errorMessage.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(password) == true ) {
			errorMessage.add("パスワードを入力してください");
		}
		if(password.matches("^[^ -~｡-ﾟ]{6,20}$") == true) {
			errorMessage.add("パスワードは記号を含む半角英数字6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isEmpty(passwordCheck) == true ) {
			errorMessage.add("確認用パスワードを入力してください");
		}
		if(!(password.equals(passwordCheck))) {
			errorMessage.add("パスワードが一致していません");
		}
		if(StringUtils.isEmpty(name) == true ) {
			errorMessage.add("名前を入力してください");
		}
		if(name.length() >= 10) {
			errorMessage.add("名前は10文字以内で入力してください");
		}
		if(errorMessage.size() ==0) {
			return true;
		}else {
			return false;
	    }

	}

}
