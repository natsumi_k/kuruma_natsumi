package chapter7.beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserComment implements Serializable  {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginId;
    private String name;
    private int userId;
    private int postId;
    private String text;
    private Timestamp created_date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Timestamp getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Timestamp createdDate) {
		this.created_date = createdDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



}
