<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta  http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>ユーザー登録</title>
		</head>
		<body>
		<div class="user-signup">
			 <c:if test="${ not empty errorMessage }">
                <div class="errorMessage">
                    <ul>
                        <c:forEach items="${errorMessage}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessage" scope="session" />
            </c:if>
         </div>

			<form action ="signup" method="post">
				<br /><label for="loginId">ログインID</label>
					<input name="loginId" id="loginId" value="${user.loginID}" /><br />
				<br /><label for="password">パスワード</label>
					<input name="password" type="password" id="password" /><br />
				<br /><label for="password_check">パスワード確認用</label>
					<input name="password_check" type="password" id="password_check" /><br />
				<br /><label for="name">ユーザー名</label>
					<input name="name" id="name" value="${user.name}"/><br />

					支店名<br>
					<select name="branchId">
						<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}">${branch.name}</option>
						</c:forEach>
					</select>
					<br><br>

					部署・役職名<br>
					<select name="departmentId">
						<c:forEach items="${departments}" var="department">
						<option value="${department.id}">${department.name}</option>
						</c:forEach>
					</select>
					<br><br>
				<br /> <input type="submit" value="新規登録" /><br /><a href="userManagement">戻る</a>
            </form>
	</body>
</html>