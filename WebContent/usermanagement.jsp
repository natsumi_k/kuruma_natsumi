<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title>ユーザー管理</title>
		<script type="text/javascript">
			function StopCheck(){
				if(confirm("停止してよろしいですか？")){
					//okなら停止を実行
					return true;
				}else{
					//キャンセルなら実行しない
					return false;
				}
			}
			function MoveCheck(){
				if(confirm("復活してよろしいですか？")){
					//okなら復活を実行
					return true;
				}else{
					//キャンセルなら実行しない
					return false;
				}
			}
		</script>
	</head>
		<body>
			<div class="usermanagement-contents">
				<a href="signup">新規ユーザー登録</a>
					</div>

					<div class="users">
						 <table>
							 <tr>
							 	<th>ログインID</th>
							 	<th>名前</th>
							 	<th>支店名</th>
							 	<th>部署・役職名</th>
							 	<th>アカウント状態</th>
							 <tr>
								 <c:forEach items="${users}"  var="user">
								 <tr>
								 	<td>${user.loginID}</td>
								 	<td>${user.name}</td>
								 	<c:forEach items="${branches}" var="branch">
								 	<td><c:if test="${user.branchId == branch.id}">
		            				<div class="branchname">
			                    		<span class="name"><c:out value="${branch.name}" /></span>
			                    	</div>
	            					</c:if>
	            					</c:forEach>

								 	<c:forEach items="${departments}" var="department">
								 	<td><c:if test="${user.departmentId == department.id}">
		            				<div class="departmentname">
			                    		<span class="name"><c:out value="${department.name}" /></span>
			                    	</div>
	            					</c:if>
	            					</c:forEach>
								 	<td><input type="hidden" value="${user.id}">
								 	<td><input type="hidden" value="${user.isWorking}">

										<form action="userState" method="post">
											<input type="hidden" name="userstateid" value="${user.id}" id="id" />
										 		<c:if test="${user.isWorking == 1}">
													<input type="hidden" name="isWorking" value="0"/>
													<input type="submit" value="停止" name="stop" onclick="return StopCheck()" /><br />
					           					</c:if>
					           					<c:if test="${user.isWorking == 0}">
													<input type="hidden" name="isWorking" value="1"/>
													<input type="submit" value="復活" onclick="return MoveCheck()" /><br />
					           					</c:if>
					          			</form>
								 		<form action="editUser" method="get">
											 <input type="hidden" name="editid" value="${user.id}" id="id" />
											 <input type="submit" value="編集する" />
										</form>
								 </tr>


						         </c:forEach>
						</table>
	               <a href="./">戻る</a>
			</div>
	</body>
</html>