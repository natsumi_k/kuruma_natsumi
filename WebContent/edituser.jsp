<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
		<title>${signUpUser.account}の設定</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
		</script>
	</head>
		<body>
			<div class="main-contents">

					<c:if test="${ not empty errorMessage}">
                		<div class="errorMessage">
                    		<ul>
                        		<c:forEach items="${errorMessage}" var="errorMessage">
                            		<li><c:out value="${errorMessage}" />
                       			 </c:forEach>
                    		</ul>
                		</div>
                		<c:remove var="errorMessage" scope="session"/>
            		</c:if>

					<form action="editUser" method="post"><br />
					<input name="id" value="${editUser.id}" id="id" type="hidden"/>
					<label for="name">ログインID</label>
					<input name="loginID" value="${editUser.loginID}" /><br />

					<label for="name">パスワード</label>
					<input name="password" type="password" /><br />

					<label for="name">確認用パスワード</label>
					<input name="password_check" type="password" /><br />

					<label for="name">名前</label>
					<input name="name" value="${editUser.name}" /><br />

					支店名<br>
					<select name="branchId">
						<c:forEach items="${branches}" var="branch" >
						<option value="${branch.id}">${branch.name}</option>
						</c:forEach>
					</select>
					<br><br>


					部署・役職名<br>
					<select name="departmentId">
						<c:forEach items="${departments}" var="department">
						<option value="${department.id}" >${department.name}</option>
						</c:forEach>
					</select>
					<br><br>

					<label for="name">アカウント状態</label>
					<c:if test="${editUser.isWorking == 1}">
						<label for="name">利用可</label>
		            </c:if>
		            <c:if test="${editUser.isWorking == 0}">
						<label for="name">利用不可</label>
		            </c:if>
					<br>

					<a href="userManagement"><input type="submit" value="登録" /> <br /></a>
                	<a href="userManagement">戻る</a>
				</form>
			</div>

		</body>
</html>