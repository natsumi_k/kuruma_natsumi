<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta  http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>

	</head>
	<body>
		<div class="postError">
			 <c:if test="${ not empty errorMessage }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessage}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
		<div class="form-area">
    		<form action="newPost" method="post">
				 <label for="subject">件名</label>
				 <input name="subject" type="text" id="subject" value="${post.subject}" /><br />
             	 本文:<br>
             	 <textarea name="text" rows="4" cols="40" >${post.text}</textarea><br>
             	 <br /><label for="category">カテゴリー</label>
             	 <input name="category" type="text" id="category" value="${post.category}" /><br />
                 <input type="submit" value="投稿" /><br />
           	<a href="index.jsp">戻る</a>
           	</form>

            <div class="copyright">Copyright(c)Kuruma Natsumi</div>

		</div>
		</div>
	</body>
</html>