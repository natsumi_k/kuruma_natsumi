<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板</title>
    </head>
    <body>

        <div class="main-contents">
            <div class="header">
                <a href="newPost">新規投稿</a>
                <a href="userManagement">ユーザー管理</a>
                <a href="logout">ログアウト</a>
            </div>

            <div class="search">
            <form action="index.jsp" method="get">

				<input type="date" name="start" /><br />
				<input type="date" name="end" /><br />
				<input type="text" name="category"><br />
				<input type="submit" value="検索" /><br />


			</form>

			</div>

           	<div class="posts">
           		<c:forEach items="${posts}" var="post">
	            		<div class="post">
	                		<div class="login_id-name">
	                    		<span class="login_id"><c:out value="${post.login_id}" /></span>
	                    		<span class="name"><c:out value="${post.name}" /></span>
	                		</div>
		                	<div class="subject"><c:out value="${post.subject}" /></div>
		                	<div class="text"><c:out value="${post.text}" /></div>
		                	<div class="category"><c:out value="${post.category}" /></div>
		                	<div class="date"><fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
		                		<form action="postDelete" method="post">
		                			<input type="hidden" name="id" value="${post.id}">
		                			<input type="submit" value="投稿削除" /><br />
		                		</form>
	            		</div>
	            		<div class="comments">

	            				<c:forEach items="${comments}" var="comment">
	            				<div class="comment">
	            				<c:if test="${post.id == comment.postId}">
		            				<div class="login_id-name">
			                    		<span class="login_id"><c:out value="${comment.loginId}" /></span>
			                    		<span class="name"><c:out value="${comment.name}" /></span>
			                    	</div>
		 							<div class="text"><c:out value="${comment.text}" /></div>
		 							<div class="date"><fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

									<form action="commentDelete" method="post">
										<input type="hidden" name="id" value="${comment.id}"/>
										<input type="submit" value="コメント削除" /><br />
									</form>
	            				</c:if>
	            				</div>
	            				</c:forEach>


	   							<form action="comment" method="post">
	             	 				<textarea name="text" rows="4" cols="40">${comment.text}</textarea><br>
	             	 				<input type="hidden" name="post_id" value="${post.id}">
	                 				<input type="submit" value="コメント" /><br />

                 				</form>
                 				<div class="main-contents">
						</div>
					<c:if test="${ not empty errorMessages}">
                		<div class="errorMessage">
                    		<ul>
                        		<c:forEach items="${errorMessages}" var="errorMessages">
                            		<li><c:out value="${errorMessages}" />
                       			 </c:forEach>
                    		</ul>
                		</div>
                		<c:remove var="errorMessage" scope="session"/>
            		</c:if>
                 		</div>
    			</c:forEach>
    		</div>
    		</div>
            <div class="copyright"> Copyright(c)Kuruma Natsumi</div>
    </body>
</html>
